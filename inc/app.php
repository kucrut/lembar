<?php declare( strict_types = 1 );

namespace Lembar\App;

use HM\JS;
use Lembar\Data;

/**
 * Bootstrapper
 */
function bootstrap(): void {
	add_action( 'lembar_render', __NAMESPACE__ . '\\render', 1 );
}

/**
 * Generate app from SSR JS
 *
 * This executes our server bundle JS and return it as markup string.
 *
 * Returns NULL if the bundle file doesn't exist.
 *
 * @return string|NULL
 */
function generate(): ?string {
	$file = dirname( __DIR__ ) . '/things/dist/server.js';

	if ( ! file_exists( $file ) ) {
		return null;
	}

	$data = Data\get_initial_data();
	$location = $_SERVER['REQUEST_URI'];
	$script = sprintf(
		'var location = %s; var __lembar_data__ = %s; %s',
		wp_json_encode( $location ),
		wp_json_encode( $data ),
		// phpcs:ignore
		file_get_contents( $file )
	);

	$compiled = JS\exec_js( $script );

	return json_decode( $compiled );
}

/**
 * Render app
 *
 * This will try to generate the app with server script via V8JS.
 * If successful, the markup will be printed, wrapped with `div#root`.
 * When the browser script kicks in, it'll rehydrate the app.
 *
 * Otherwise, an empty div#root will be printed and it will be populated by
 * the browser script.
 */
function render(): void {
	$app = generate();

	if ( empty( $app ) ) {
		echo '<div id="root"></div>';

		return;
	}

	printf(
		'<div id="root" data-rendered="">%s</div>',
		$app,
	);
}
