<?php declare( strict_types = 1 );

namespace Lembar\Data;

use WP;
use WP_Post;
use WP_Query;
use WP_REST_Request;
use WP_Term;

/**
 * Bootsrapper
 */
function bootstrap(): void {
	add_action( 'wp', __NAMESPACE__ . '\\collect_data' );
}

/**
 * Strip site URL from string
 *
 * @param string $string String containing site URL.
 *
 * @return string
 */
function strip_site_url( string $string ): string {
	return str_replace( home_url( '/' ), '/', $string );
}

/**
 * Make API request
 *
 * @param string $endpoint API endpoint.
 *
 * @return array<mixed> Response data.
 */
function request( string $endpoint ): array {
	$server   = rest_get_server();
	$request  = new WP_REST_Request( 'GET', $endpoint );
	$response = $server->dispatch( $request );

	return $response->get_data();
}

/**
 * Get post data
 *
 * @param WP_Post $post Post object.
 *
 * @return array<mixed> Post data.
 */
function get_post_data( WP_Post $post ): array {
	$server     = rest_get_server();
	$request    = new WP_REST_Request();
	$pt_object  = get_post_type_object( $post->post_type );
	$controller = new $pt_object->rest_controller_class( $post->post_type );
	$response   = $controller->prepare_item_for_response( $post, $request );

	if ( ! $response || is_wp_error( $response ) || $response->is_error() ) {
		return null;
	}

	$data = $server->response_to_data( $response, true );
	$data['routePath'] = strip_site_url( $data['link'] );

	return $data;
}

/**
 * Flatten terms array
 *
 * @param array<int> $carry Current value.
 * @param array<int> $terms Current terms.
 *
 * @return array<int>
 */
function flatten_terms( array $carry, array $terms ): array {
	return array_unique(
		array_merge(
			$carry,
			$terms,
		),
	);
}

/**
 * Get term data
 *
 * @param WP_Term $term Term object.
 *
 * @return array<mixed>|null Term data or NULL on error.
 */
function get_term_data( WP_Term $term ): ?array {
	$server     = rest_get_server();
	$request    = new WP_REST_Request();
	$tax_object  = get_taxonomy( $term->taxonomy );
	$controller_class = $tax_object->rest_controller_class;

	if ( empty( $controller_class ) || ! class_exists( $controller_class ) ) {
		return null;
	}

	$controller = new $tax_object->rest_controller_class( $term->taxonomy );
	$response   = $controller->prepare_item_for_response( $term, $request );

	if ( ! $response || is_wp_error( $response ) || $response->is_error() ) {
		return null;
	}

	$data = $server->response_to_data( $response, true );

	unset( $data['_links'] );
	$data['routePath'] = strip_site_url( $data['link'] );

	return $data;
}

/**
 * Get initial posts
 *
 * @param WP_Query $query      WP Query object.
 * @param string   $archive_id Archive ID.
 *
 * @return array<mixed>
 */
function get_initial_posts( WP_Query $query, string $archive_id ): array {
	if ( $query->is_attachment() || $query->is_page() || $query->is_404() || $query->is_preview() ) {
		return [
			'posts' => [],
		];
	}

	$posts = array_map( __NAMESPACE__ . '\\get_post_data', $query->posts );
	$ids = wp_list_pluck( $posts, 'id' );
	$page_num = $query->get( 'paged' ) > 1 ?
		$query->get( 'paged' )
		: 1;

	$result = [
		'posts' => $posts,
	];

	if ( empty( $archive_id ) ) {
		return $result;
	}

	$result = array_merge(
		$result,
		[
			// For repress's withArchive() HOC.
			'archives' => [
				$archive_id => $ids,
			],
			// For repress's withPagedArchive() HOC.
			'page' => $page_num,
			'archivesByPage' => [
				$archive_id => [
					$page_num => $ids,
				],
			],
			'archivePages' => [
				$archive_id => [
					'current' => $page_num,
					'total' => absint( $query->max_num_pages ),
				],
			],
		],
	);

	return $result;
}

/**
 * Get initial page
 *
 * @param WP_Query $query WP Query object.
 *
 * @return array<mixed>
 */
function get_initial_page( WP_Query $query ): array {
	return [
		'posts' => $query->is_page()
			? [ get_post_data( $query->post ) ]
			: [],
	];
}

/**
 * Get initial media
 *
 * @param WP_Query $query WP Query object.
 *
 * @return array<mixed>
 */
function get_initial_media( WP_Query $query ): array {
	return [
		'posts' => $query->is_attachment()
			? [ get_post_data( $query->post ) ]
			: [],
	];
}

/**
 * Get info data
 *
 * @return array<mixed>
 */
function get_info(): array {
	$upload_dir = wp_get_upload_dir();

	$info = [
		'apiUrl'        => untrailingslashit( get_rest_url(), '/' ),
		'baseUploadUrl' => $upload_dir['baseurl'],
		'description'   => get_bloginfo( 'description' ),
		'name'          => get_bloginfo( 'name' ),
		'url'           => strip_site_url( home_url() ),
		'settings'      => [
			'posts_per_page' => absint( get_option( 'posts_per_page' ) ),
		],
	];

	/**
	 * Filter initial info data
	 *
	 * @param $info array Info data.
	 */
	$info = apply_filters( 'lembar_initial_data_info', $info );

	return $info;
}

/**
 * Rebuild menu item
 *
 * @param array<mixed> $item Item data.
 *
 * @return array<mixed>
 */
function rebuild_menu_item( array $item ): array {
	$item['url'] = strip_site_url( $item['url'] );
	$item['subItems'] = array_map( __NAMESPACE__ . '\\rebuild_menu_item', $item['children'] );

	unset( $item['children'] );

	return $item;
}

/**
 * Get menus
 *
 * @return array<mixed>
 */
function get_menus(): array {
	$menu_locations = array_keys( get_registered_nav_menus() );
	$menus = [];

	foreach ( $menu_locations as $location ) {
		$menu = request( "/bridge/v1/menus/{$location}" );

		if ( empty( $menu ) ) {
			continue;
		}

		$menu['isOpen']   = false;
		$menu['items']    = array_map( __NAMESPACE__ . '\\rebuild_menu_item', $menu['items'] );
		$menu['location'] = $location;

		$menus[ $location ] = $menu;
	}

	/**
	 * Filter menus
	 *
	 * @param array $menus Menus.
	 */
	$menus = apply_filters( 'lembar_menus', $menus );

	return $menus;
}

/**
 * Get taxonomies
 *
 * @return array<mixed>
 */
function get_taxonomies(): array {
	global $wp_rewrite;

	$filter = static function ( $response ) use ( $wp_rewrite ) {
		$data = $response->get_data();
		$slug = $data['slug'];

		$data['routePath'] = str_replace(
			"%{$slug}%",
			":{$data['rest_base']}",
			$wp_rewrite->extra_permastructs[ $slug ]['struct'],
		);

		$response->set_data( $data );

		return $response;
	};

	add_filter( 'rest_prepare_taxonomy', $filter );
	$taxonomies = request( '/wp/v2/taxonomies' );
	remove_filter( 'rest_prepare_taxonomy', $filter );

	/**
	 * Filter taxonomies for route
	 *
	 * @param array $taxonomies Array of taxonomy route data.
	 */
	$taxonomies = apply_filters( 'lembar_taxonomies', $taxonomies );

	return $taxonomies;
}

/**
 * Get post types
 *
 * @return array<mixed>
 */
function get_types(): array {
	$types = request( '/wp/v2/types' );

	if ( empty( $types ) || is_wp_error( $types ) ) {
		return [];
	}

	unset( $types['wp_block'] );

	return $types;
}

/**
 * Get post formats
 *
 * @return array<mixed>
 */
function get_formats(): array {
	$filter = static function ( $response ) {
		$data = $response->get_data();
		$data['routePath'] = strip_site_url( $data['link'] );

		$response->set_data( $data );

		return $response;
	};

	add_filter( 'rest_prepare_post_format', $filter );
	$formats = request( '/wp/v2/formats' );
	remove_filter( 'rest_prepare_post_format', $filter );

	if ( empty( $formats ) || is_wp_error( $formats ) ) {
		return [];
	}

	return $formats;
}

/**
 * Get initial archive params
 *
 * @param WP_Query $query WP Query object.
 *
 * @return array<mixed>
 */
function get_initial_archive_params( WP_Query $query ): array {
	if ( $query->is_category() || $query->is_tag() || $query->is_tax() ) {
		$term = get_queried_object();
		$taxonomy = get_taxonomy( $term->taxonomy );

		return [
			$taxonomy->rest_base => [ $term->term_id ],
		];
	}

	// TODO: Date archives.

	return [];
}

/**
 * Create initial archive ID
 *
 * @param WP_Query $query WP Query object.
 * @param string   $path    Request path.
 *
 * @return string|null
 */
function get_initial_archive_id( WP_Query $query, string $path ): ?string {
	if ( $query->is_home() ) {
		return 'home';
	}

	if ( $query->is_search() ) {
		return sprintf( 'search/%s', $query->get( 's' ) );
	}

	if ( $query->is_archive() ) {
		return trim( preg_replace( '#page/(\d+)#', '', $path ), '/' );
	}

	return null;
}

/**
 * Get initial terms
 *
 * This collects the term objects of posts in the main query so we can use them
 * in the components without having to wait for the async API requests to finish.
 *
 * @param array<mixed> $taxonomies Array of taxonomies.
 * @param array<mixed> $posts      Array of initial posts.
 *
 * @return array<mixed>
 */
function get_initial_terms( array $taxonomies, array $posts ): array {
	$result = [];

	if ( empty( $posts ) ) {
		return $result;
	}

	foreach ( $taxonomies as $taxonomy ) {
		$tax = $taxonomy['rest_base'];

		if ( $tax === 'formats' ) {
			continue;
		}

		$term_ids = array_reduce(
			array_column( $posts, $tax ),
			__NAMESPACE__ . '\\flatten_terms',
			[],
		);

		if ( empty( $term_ids ) ) {
			continue;
		}

		$terms = get_terms(
			[
				'taxonomy' => $taxonomy['slug'],
				'include'  => $term_ids,
			],
		);

		$result[ $tax ] = [
			'posts' => array_map( __NAMESPACE__ . '\\get_term_data', $terms ),
		];
	}

	return $result;
}

/**
 * Collect data
 *
 * When WordPress is fully loaded on the frontend, we collect data needed
 * by the app for the server and browser scripts.
 *
 * @param WP $wp WP object.
 */
function collect_data( WP $wp ): void {
	global $wp_query;
	global $wp_rewrite;

	// Data is only needed on the frontend.
	if ( is_admin() ) {
		return;
	}

	$archive_id    = get_initial_archive_id( $wp_query, $wp->request ) ?? '';
	$is_search     = $wp_query->is_search();
	$initial_posts = get_initial_posts( $wp_query, $archive_id );
	$taxonomies    = get_taxonomies();

	// Add posts from search result to their own type's collection.
	if ( $is_search ) {
		$media = [
			'posts' => wp_list_filter(
				$initial_posts['posts'],
				[
					'type' => 'attachment',
				],
			),
		];

		$pages = [
			'posts' => wp_list_filter(
				$initial_posts['posts'],
				[
					'type' => 'page',
				],
			),
		];

		$posts = [
			'posts' => wp_list_filter(
				$initial_posts['posts'],
				[
					'type' => 'post',
				],
			),
		];
	} else {
		$media = get_initial_media( $wp_query );
		$pages = get_initial_page( $wp_query );
		$posts = $initial_posts;
	}

	$data = array_merge(
		[
			'formats' => [
				'posts' => get_formats(),
			],
			'info'       => get_info(),
			'menus'      => get_menus(),
			'media'      => $media,
			'pages'      => $pages,
			'posts'      => $posts,
			'search'     => $is_search ? $initial_posts : [],
			'taxonomies' => $taxonomies,
			'types'      => get_types(),
			'routes'      => [
				'author' => $wp_rewrite->author_base,
				'front'  => $wp_rewrite->front,
				'paged'  => $wp_rewrite->pagination_base,
				'search' => $wp_rewrite->search_base,
				'is404'  => $wp_query->is_404(),
			],
		],
		get_initial_terms( $taxonomies, $initial_posts['posts'] ),
	);

	add_filter(
		'lembar_initial_data',
		static function ( $script_data ) use ( $data ) {
			return array_merge( $script_data, $data );
		},
	);
}

/**
 * Get initial data
 *
 * @return array<mixed> $data Initial data.
 */
function get_initial_data(): array {
	/**
	 * Filter initial data
	 *
	 * @param array $data Initial data.
	 */
	return apply_filters( 'lembar_initial_data', [] );
}
