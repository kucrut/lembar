const path = require( 'path' );
const { HashedModuleIdsPlugin } = require( 'webpack' );
const merge = require( 'webpack-merge' );

const { createEntries } = require( './utils' );
const getSharedConfig = require( './shared' );
const { getCSSConfig } = require( './css' );

const resolve = {
	alias: {
		malimbu: path.resolve( process.cwd(), 'things', 'src', 'malimbu' ),
	}
};

/**
 * Get frontend config for browsers
 *
 * @param {Object} args Args passed to webpack.
 *
 * @return {Object}
 */
function getBrowserConfig( args ) {
	const { dev: isDevelopment = false, wds: withWds = false } = args;

	return merge(
		getCSSConfig( {
			...args,
			postCSSPlugins: ( loader, plugins ) => [
				require( 'postcss-import' )( {
					path: path.resolve( process.cwd(), 'things', 'src', 'styles' ),
				} ),
				require( 'postcss-simple-vars' ),
				require( 'postcss-mixins' )( {
					mixinsDir: path.resolve( process.cwd(), 'things', 'src', 'styles', 'mixins' ),
				} ),
				...plugins,
			],
		} ),
		getSharedConfig( args ),
		createEntries( 'browser', 'things', isDevelopment, { withWds } ),
		{
			plugins: [
				new HashedModuleIdsPlugin(),
			],
			optimization: {
				runtimeChunk: 'single',
				splitChunks: {
					chunks: 'all',
				},
			},
		},
		{ resolve },
	);
}

/**
 * Get frontend config for server
 *
 * @param {Object} args Args passed to webpack.
 *
 * @return {Object}
 */
function getSSRConfig( args ) {
	const { dev: isDevelopment = false } = args;

	return merge(
		getSharedConfig( {
			...args,
			rhl: false,
		} ),
		createEntries( 'server', 'things', isDevelopment, {
			isSSR: true,
		} ),
		{
			resolve,
			module: {
				rules: [
					{
						test: /\.p?css$/,
						use: [ 'ignore-loader' ],
					},
				],
			},
			performance: false,
		},
	);
}

/**
 * Get all theme configs
 *
 * @param {Object} args Args passed to webpack.
 *
 * @return {Object}
 */
function getConfigs( args ) {
	const finalArgs = {
		dev: false,
		rhl: false,
		wds: false,
		...args,
	};

	return [
		getBrowserConfig( finalArgs ),
		getSSRConfig( finalArgs ),
	];
}

module.exports = getConfigs;
