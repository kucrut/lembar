import { hot } from 'react-hot-loader';

import { generateDocTitle } from '../../utils/helpers';
import { EntryList, withArchiveRoute, withDocTitle } from '../../components';

function mapPropsToDocTitle( props ) {
	const { page, siteDescription, siteName } = props;
	const title = generateDocTitle( siteName, siteDescription, page );

	return title;
}

const Home = withArchiveRoute( 'posts', {} )(
	withDocTitle( mapPropsToDocTitle )( EntryList )
);

export default hot( module )( Home );
