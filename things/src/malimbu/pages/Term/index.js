import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader';

import { routeShape } from '../../shapes';
import { getHandler } from '../../store/types';
import { generateDocTitle } from '../../utils/helpers';
import {
	EntryList,
	with404,
	withDocTitle,
	withArchiveRoute,
	withPageLoader,
	withSingleBySlug,
} from '../../components';

function mapPropsToDocTitle( props ) {
	const { page, siteName, post: term } = props;

	if ( ! term ) {
		return null;
	}

	const { name } = term;
	const title = generateDocTitle( siteName, '', page, name );

	return title;
}

function mapPropsToArchiveParams( props ) {
	const { taxonomy, post: term } = props;

	if ( ! term ) {
		return {};
	}

	return {
		[ taxonomy ]: term.id,
	};
}

function mapPropsToSlug( props ) {
	const { match, taxonomy } = props;
	const { params } = match;
	const slug = taxonomy === 'formats'
		? `post-format-${ params[ taxonomy ] }`
		: params[ taxonomy ];

	return slug;
}

export function Term( props ) {
	const { taxonomy } = props;

	const WithDocTitle = withDocTitle( mapPropsToDocTitle )( EntryList );
	const WithArchive = withArchiveRoute( 'posts', mapPropsToArchiveParams )( WithDocTitle );
	const Content = withPageLoader(
		( { loading, post } ) => ( loading || ! post )
	)( WithArchive );

	const Component = withSingleBySlug(
		getHandler( taxonomy ),
		state => state[ taxonomy ],
		mapPropsToSlug
	)( Content );

	return <Component { ...props } />;
}

Term.propTypes = {
	...routeShape,
	taxonomy: PropTypes.string.isRequired,
};

export default hot( module )( with404()( Term ) );
