import React from 'react';

import { collectNodeAttributes } from '../../../utils/helpers';

export default function img( siteInfo, node ) {
	const loading = node.getAttribute( 'loading' );

	if ( loading ) {
		return;
	}

	// eslint-disable-next-line jsx-a11y/alt-text
	return <img loading="lazy" { ...collectNodeAttributes( node ) } />;
}
