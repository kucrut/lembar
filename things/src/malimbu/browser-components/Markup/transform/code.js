import React from 'react';
import classnames from 'classnames';
import decode from 'simple-entity-decode';

import {
	collectNodeAttributes,
	getCodeLanguage,
	getPrismComponents,
} from '../../../utils/helpers';
import Prism from '../../Prism';

export default function code( siteInfo, node ) {
	if ( node.parentNode.tagName !== 'PRE' ) {
		return;
	}

	const lang = getCodeLanguage( node.className ) || getCodeLanguage( node.parentNode.className );
	const language = getPrismComponents().indexOf( lang ) >= 0 ? lang : 'none';
	const props = {
		...collectNodeAttributes( node ),
		language,
		className: classnames( [
			...Array.from( node.classList ),
			`language-${ language }`,
		] ),
	};

	return (
		<Prism { ...props }>
			{ decode( node.innerText ) }
		</Prism>
	);
}
