import { trim } from 'lodash';
import { withSingle } from '@humanmade/repress';

import { getHandler } from '../store/types';

const attrMap = {
	'class': 'className',
	'for': 'htmlFor',
	'srcset': 'srcSet',
};
const boolAttrs = [
	'disabled',
	'required',
];

export function createSingleHOC( type, id ) {
	const handler = getHandler( type );

	return withSingle( handler, state => state[ type ], id );
}

export function generateArchiveId( url ) {
	return trim( url.replace( /\/page\/(\d+)/, '' ), '/' ) || 'home';
}

export function generateDocTitle( siteName, siteDescription, page, prefix ) {
	let title = prefix ? prefix : siteName;

	if ( page > 1 ) {
		title = `${ title } – Page ${ page }`;
	}

	title = prefix
		? `${ title } – ${ siteName }`
		: `${ title } – ${ siteDescription }`;

	return title;
}

export function resolve( maybeFunc, ...args ) {
	return typeof maybeFunc === 'function'
		? maybeFunc( ...args )
		: maybeFunc;
}

export function injectScript( props ) {
	const { onLoaded, src, ...rest } = props;
	const el = document.createElement( 'script' );

	el.src = src;
	el.type = 'text/javascript';

	Object.keys( rest ).forEach( key => el.setAttribute( key, rest[ key ] ) );
	document.head.appendChild( el );

	el.onload = onLoaded;

	return el;
}

export function injectStyle( props ) {
	const { onLoaded, src, ...rest } = props;
	const el = document.createElement( 'link' );

	el.href = src;
	el.rel = 'stylesheet';
	el.type = 'text/css';

	Object.keys( rest ).forEach( key => el.setAttribute( key, rest[ key ] ) );
	document.head.appendChild( el );

	el.onload = onLoaded;

	return el;
}

/**
 * Prism components
 *
 * NOTE: Order is important!
 */
export function getPrismComponents() {
	return [
		'markup-templating',
		'clike',
		'javadoclike',
		'javascript',
		'php',
		'php-extras',
		'bash',
		'phpdoc',
		'apacheconf',
		'c',
		'cpp',
		'css-extras',
		'docker',
		'git',
		'go',
		'graphql',
		'js-extras',
		'json',
		'json5',
		'jsonp',
		'jsx',
		'markdown',
		'nginx',
		'regex',
		'scss',
		'sql',
		'tsx',
		'typescript',
		'yaml',
	];
}

/**
 * Prism script from CDN
 */
export function getPrismScriptSrc() {
	return 'https://cdn.jsdelivr.net/combine/npm/prismjs@1.17.1'
		+ getPrismComponents().reduce( ( carry, comp ) => `${ carry },npm/prismjs@1.17.1/components/prism-${ comp }.min.js`, '' );
}

export function collectNodeAttributes( node, excludes = [] ) {
	const attributes = {};

	node.getAttributeNames().forEach( attr => {
		const key = attrMap[ attr ] || attr;

		if ( excludes.indexOf( attr ) >= 0 ) {
			return;
		}

		let value = node.getAttribute( attr );

		if ( boolAttrs.includes( key ) && value === '' ) {
			value = true;
		}

		attributes[ key ] = value;
	} );

	return attributes;
}

export function getCodeLanguage( str ) {
	const match = str.match( /\blanguage-([a-z]+)\b/ );

	if ( ! match ) {
		return null;
	}

	return match[1];
}
