import configureStore from '../store';
import createRoutes from './routes';

export default function init( initialState, options = {} ) {
	const {
		extraReducers = () => {},
		pageComponents = {},
		storeMiddlewares = [],
	} = options;

	const store = configureStore( initialState, storeMiddlewares, extraReducers );
	const { routes, taxonomies } = store.getState();
	const appRoutes = createRoutes( routes, taxonomies, pageComponents );

	return {
		appRoutes,
		store,
	};
}
