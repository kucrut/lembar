import PropTypes from 'prop-types';

export default {
	description: PropTypes.string.isRequired,
	id: PropTypes.number.isRequired,
	name: PropTypes.string.isRequired,
	parent: PropTypes.number,
	routePath: PropTypes.string.isRequired,
	slug: PropTypes.string.isRequired,
};
