export const TOGGLE_EXTERNAL_ASSET = 'TOGGLE_EXTERNAL_ASSET';

export default function externalAssets( state = {}, action ) {
	const { type, id, loaded = false } = action;

	switch ( type ) {
		case TOGGLE_EXTERNAL_ASSET: {
			return {
				...state,
				[ id ]: {
					loaded,
				},
			};
		}

		default:
			return state;
	}
}
