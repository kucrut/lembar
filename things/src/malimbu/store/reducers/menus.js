export const TOGGLE_MENU = 'TOGGLE_MENU';

export default function menus( state = {}, action ) {
	const { type } = action;

	switch ( type ) {
		case TOGGLE_MENU: {
			const { location, isOpen } = action;

			return {
				...state,
				[ location ]: {
					...state[ location ],
					isOpen,
				},
			};
		}

		default:
			return state;
	}
}
