function addRoutePath( item, siteUrl ) {
	return {
		...item,
		routePath: item.link.replace( siteUrl, '' ),
	};
}

function middleware( actionDataMap, siteUrl ) {
	return next => action => {
		const { type } = action;

		if ( ! ( type in actionDataMap ) ) {
			return next( action );
		}

		const dataKey = actionDataMap[ type ];
		const data = action[ dataKey ];

		if ( ! data ) {
			return next( action );
		}

		return next( {
			...action,
			[ dataKey ]: Array.isArray( data )
				? data.map( item => addRoutePath( item, siteUrl ) )
				: addRoutePath( data, siteUrl ),
		} );
	};
}

export default function addRoutePathMiddleware( initialState ) {
	const { info, taxonomies, types } = initialState;
	const { url: siteUrl } = info;
	const actionDataMap = {};

	Object.values( types ).forEach( ( { rest_base: type } ) => {
		actionDataMap[ `LOAD_${ type.toUpperCase() }` ] = 'data';
		actionDataMap[ `LOAD_${ type.toUpperCase() }_BY_SLUG` ] = 'data';
		actionDataMap[ `QUERY_${ type.toUpperCase() }` ] = 'results';
		actionDataMap[ `QUERY_${ type.toUpperCase() }_MORE` ] = 'results';
	} );

	Object.values( taxonomies ).forEach( ( { rest_base: tax } ) => {
		actionDataMap[ `LOAD_${ tax.toUpperCase() }` ] = 'data';
		actionDataMap[ `LOAD_${ tax.toUpperCase() }_BY_SLUG` ] = 'data';
		actionDataMap[ `QUERY_${ tax.toUpperCase() }` ] = 'results';
		actionDataMap[ `QUERY_${ tax.toUpperCase() }_MORE` ] = 'results';
	} );

	return ( ...args ) => middleware( actionDataMap, siteUrl, ...args );
}
