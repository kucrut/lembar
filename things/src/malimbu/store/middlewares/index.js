import addRoutePathMiddleware from './add-route-path';

export default function createMiddlewares( initialState ) {
	return [
		addRoutePathMiddleware( initialState ),
	];
}
