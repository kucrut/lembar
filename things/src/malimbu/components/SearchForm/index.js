import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { __ } from '@wordpress/i18n';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export function SearchForm( props ) {
	const {
		buttonLabel,
		className,
		history,
		id,
		label,
		onSubmit,
		placeholder,
		siteUrl,
	} = props;

	const inputEl = useRef( null );

	const handleSubmit = e => {
		const { value } = inputEl.current;

		e.preventDefault();

		if ( ! value ) {
			return;
		}

		history.push( `/search/${ encodeURIComponent( value ) }` );
		onSubmit();
	};

	return (
		<form
			action={ siteUrl }
			className={ className }
			id={ id }
			method="get"
			onSubmit={ handleSubmit }
		>
			<label className={ `${ id }-label` } htmlFor={ `${ id }-input` }>
				{ label }
			</label>
			<input
				className={ `${ id }-input` }
				id={ `${ id }-input` }
				name="s"
				placeholder={ placeholder }
				ref={ inputEl }
				type="search"
			/>
			<button className={ `${ id }-button` } type="submit">{ buttonLabel }</button>
		</form>
	);
}

SearchForm.defaultProps = {
	buttonLabel: __( 'Search', 'malimbu' ),
	className: 'search-form',
	label: __( 'Search for:', 'malimbu' ),
	onSubmit: () => {},
	placeholder: '',
};

SearchForm.propTypes = {
	buttonLabel: PropTypes.node,
	className: PropTypes.string,
	history: PropTypes.shape( {
		push: PropTypes.func.isRequired,
	} ).isRequired,
	id: PropTypes.string.isRequired,
	label: PropTypes.node,
	onSubmit: PropTypes.func,
	placeholder: PropTypes.string,
	siteUrl: PropTypes.string.isRequired,
};

function mapStateToProps( state ) {
	const { info } = state;
	const { url: siteUrl } = info;

	return {
		siteUrl,
	};
}

export default withRouter( connect( mapStateToProps )( SearchForm ) );
