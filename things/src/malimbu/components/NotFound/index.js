import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { __ } from '@wordpress/i18n';

export default function NotFound( props ) {
	const { className, title } = props;
	const finalClassName = classnames( [
		'error-404',
		'not-found',
		className,
	] );

	return (
		<div className={ finalClassName }>
			<h1 className="entry-title">{ title }</h1>
			<p>Nothing found ;-(</p>
		</div>
	);
}

NotFound.defaultProps = {
	className: '',
	title: __( 'Ooops!', 'malimbu' ),
};

NotFound.propTypes = {
	className: PropTypes.string,
	title: PropTypes.string,
};
