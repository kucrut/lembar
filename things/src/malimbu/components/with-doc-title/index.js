import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import decode from 'simple-entity-decode';

import { resolve } from '../../utils/helpers';

export default function withDocTitle( mapPropsToDocTitle ) {
	return WrappedComponent => {
		function WithDocTitle( props ) {
			const title = resolve( mapPropsToDocTitle, props );

			return (
				<Fragment>
					{ title && (
						<Helmet>
							<title>{ decode( title ) }</title>
						</Helmet>
					) }
					<WrappedComponent { ...props } />
				</Fragment>
			);
		}

		WithDocTitle.propTypes = {
			siteName: PropTypes.string.isRequired,
			siteDescription: PropTypes.string.isRequired,
		};

		return connect( state => ( {
			siteName: state.info.name,
			siteDescription: state.info.description,
		} ) )( WithDocTitle );
	};
}
