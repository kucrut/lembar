import React from 'react';
import PropTypes from 'prop-types';

import { itemComponents, itemShape } from './shapes';

export default function ListItem( props ) {
	const { className, subItems, Item, Link, Text, Wrap, ...rest } = props;
	const { url } = rest;

	return (
		<li className={ className }>
			{ url
				? <Link { ...rest }><Text { ...rest } /></Link>
				: <Text { ...rest } />
			}
			{ subItems.length
				// eslint-disable-next-line object-property-newline, object-curly-newline
				? <Wrap className="sub" { ...{ Item, Link, Text, Wrap } } items={ subItems } />
				: null
			}
		</li>
	);
}

ListItem.defaultProps = {
	className: 'item',
	url: '',
	subItems: [],
};

ListItem.propTypes = {
	className: PropTypes.string,
	...itemShape,
	...itemComponents,
};
