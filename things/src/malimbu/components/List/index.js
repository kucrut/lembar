import React from 'react';
import PropTypes from 'prop-types';

import ListItem from './ListItem';
import ListItemLink from './ListItemLink';
import ListItemText from './ListItemText';
import Wrap from './Wrap';

export default function List( props ) {
	return <Wrap { ...props } />;
}

List.defaultProps = {
	className: '',
	Item: ListItem,
	Link: ListItemLink,
	Text: ListItemText,
	Wrap,
};

List.propTypes = {
	...Wrap.propTypes,
	Item: PropTypes.elementType,
};
