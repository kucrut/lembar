import React from 'react';

export default function PageLoader() {
	return <div className="page-loader">Loading&hellip;</div>;
}
