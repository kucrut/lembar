import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { postShape, termShape } from '../../shapes';
import { resolve } from '../../utils/helpers';

/**
 * withSingle HOC
 *
 * See https://github.com/humanmade/repress/blob/master/src/withSingle.js
 */
export default ( handler, getSubstate, mapPropsToSlug, options = {} ) => Component => {
	const mapDataToProps = options.mapDataToProps || ( data => data );
	const mapActionsToProps = options.mapActionsToProps || ( actions => actions );

	const {
		fetchSingleBySlug,
		getSingleBySlug,
		isPostLoadingBySlug,
		isPostSaving,
		updateSingle,
	} = handler;

	class WrappedComponent extends React.Component {
		componentDidMount() {
			const { _actions, _data } = this.props;

			if ( ! _data.post && ! _data.loading ) {
				_actions.onLoad();
			}
		}

		componentDidUpdate( prevProps ) {
			const { _actions, _data } = this.props;
			const { _data: prevData } = prevProps;

			if ( ! _data.post && prevData.postSlug !== _data.postSlug ) {
				_actions.onLoad();
			}
		}

		render() {
			const { _data, _actions, ...props } = this.props;
			const childProps = {
				...props,
				...mapDataToProps( _data, props ),
				...mapActionsToProps( _actions, props ),
			};

			return <Component { ...childProps } />;
		}
	}

	WrappedComponent.propTypes = {
		_actions: PropTypes.shape( {
			onLoad: PropTypes.func.isRequired,
		} ),
		_data: PropTypes.shape( {
			loading: PropTypes.bool.isRequired,
			post: PropTypes.oneOfType( [
				PropTypes.shape( postShape ),
				PropTypes.shape( termShape ),
			] ),
			postSlug: PropTypes.string.isRequired,
		} ).isRequired,
	};

	const mapStateToProps = ( state, props ) => {
		const substate = getSubstate( state );
		const resolvedSlug = resolve( mapPropsToSlug, props );
		const post = getSingleBySlug( substate, resolvedSlug );

		return {
			_data: {
				post,
				postSlug: resolvedSlug,
				loading: isPostLoadingBySlug( substate, resolvedSlug ),
				saving: isPostSaving( substate, resolvedSlug ),
			},
		};
	};

	const mapDispatchToProps = ( dispatch, props ) => {
		const resolvedSlug = resolve( mapPropsToSlug, props );

		return {
			_actions: {
				onLoad: ( context = 'view' ) => dispatch( fetchSingleBySlug( resolvedSlug, context ) ),
				onUpdatePost: data => dispatch( updateSingle( {
					slug: resolvedSlug,
					...data,
				} ) ),
			},
		};
	};

	return connect(
		mapStateToProps,
		mapDispatchToProps
	)( WrappedComponent );
};
