import React from 'react';

import { resolve } from '../../utils/helpers';
import PageLoader from '../PageLoader';

export default function withPageLoader( isLoading ) {
	return WrappedComponent => props => {
		const resolvedLoading = resolve( isLoading, props );

		if ( resolvedLoading ) {
			return <PageLoader />;
		}

		return <WrappedComponent { ...props } />;
	};
}
