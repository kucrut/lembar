import React from 'react';
import PropTypes from 'prop-types';

import { postShape } from '../../shapes';
import { resolve } from '../../utils/helpers';
import EntryContent from '../EntryContent';
import EntryMeta from '../EntryMeta';
import EntryTitle from '../EntryTitle';
import TermsList from '../TermsList';
import withPageLoader from '../with-page-loader';

export function Entry( props ) {
	const { className, post } = props;
	const {
		featured_media,
		format,
		id,
		status,
		type,
	} = post;

	const finalClassName = [
		resolve( className, props ),
		'hentry',
		'post',
		`post-${id}`,
		`status-${ status }`,
		`type-${ type }`,
		( format && `format-${ format }` ),
		( featured_media && 'has-post-thumbnail' ),
	].filter( Boolean ).join( ' ' );

	return (
		<article className={ finalClassName }>
			<EntryTitle { ...props } />
			<EntryContent { ...props } />
			<EntryMeta { ...props } showDate={ type === 'post' } />
			<TermsList { ...props } taxonomy="tags" />
		</article>
	);
}

Entry.defaultProps = {
	className: '',
	isSingle: false,
};

Entry.propTypes = {
	className: PropTypes.oneOfType( [
		PropTypes.func,
		PropTypes.string,
	] ),
	isSingle: PropTypes.bool,
	post: PropTypes.shape( postShape ).isRequired,
};

export default withPageLoader(
	( { loading, post } ) => ( loading || ! post )
)( Entry );
