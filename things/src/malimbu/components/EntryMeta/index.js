import React from 'react';
import PropTypes from 'prop-types';

import { createSingleHOC } from '../../utils/helpers';
import { postShape } from '../../shapes';
import TermLink from '../TermLink';
import Time from '../Time';

export default function EntryMeta( props ) {
	const { post, showDate } = props;
	const { categories, date } = post;

	if ( ! showDate && ! ( categories && categories.length ) ) {
		return null;
	}

	const Category = ( categories && categories.length )
		? createSingleHOC( 'categories', categories[0] )( TermLink )
		: null;

	return (
		<p className="entry-meta">
			{ showDate && <span className="entry-date">Posted on <Time date={ date } /></span> }
			{ ' ' }
			{ Category && <span className="entry-category">in <Category /></span> }
		</p>
	);
}

EntryMeta.propTypes = {
	post: PropTypes.shape( postShape ).isRequired,
	showDate: PropTypes.bool.isRequired,
};
