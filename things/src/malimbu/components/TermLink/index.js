import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import decode from 'simple-entity-decode';

import { termShape } from '../../shapes';

export default function TermLink( props ) {
	const { className, loading, post: term } = props;

	if ( loading || ! term ) {
		return null;
	}

	const { name, routePath } = term;

	return (
		<Link className={ className } to={ routePath }>
			{ decode( name ) }
		</Link>
	);
}

TermLink.defaultProps = {
	className: '',
};

TermLink.propTypes = {
	className: PropTypes.string,
	loading: PropTypes.bool.isRequired,
	post: PropTypes.shape( termShape ),
};
