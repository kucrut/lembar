import React from 'react';
import PropTypes from 'prop-types';

export default function Icon( props ) {
	const { icon, ...restProps } = props;
	let path;
	let viewBox = '0 0 20 20'; // default.

	switch ( icon ) {
		case 'menu':
			path =
				'M3,16.5 L3,18.5 L21,18.5 L21,16.5 L3,16.5 Z M3,10.5 L3,12.5 L21,12.5 L21,10.5 L3,10.5 Z M3,4.5 L3,6.5 L21,6.5 L21,4.5 L3,4.5 Z';
			viewBox = '0 0 24 20';
			break;
		case 'search':
			path =
				'M20.6666667,18.6666667 L19.6133333,18.6666667 L19.24,18.3066667 C20.5466667,16.7866667 21.3333333,14.8133333 21.3333333,12.6666667 C21.3333333,7.88 17.4533333,4 12.6666667,4 C7.88,4 4,7.88 4,12.6666667 C4,17.4533333 7.88,21.3333333 12.6666667,21.3333333 C14.8133333,21.3333333 16.7866667,20.5466667 18.3066667,19.24 L18.6666667,19.6133333 L18.6666667,20.6666667 L25.3333333,27.32 L27.32,25.3333333 L20.6666667,18.6666667 L20.6666667,18.6666667 Z M12.6666667,19.5 C8.88555556,19.5 5.83333333,16.4477778 5.83333333,12.6666667 C5.83333333,8.88555556 8.88555556,5.83333333 12.6666667,5.83333333 C16.4477778,5.83333333 19.5,8.88555556 19.5,12.6666667 C19.5,16.4477778 16.4477778,19.5 12.6666667,19.5 L12.6666667,19.5 Z';
			viewBox = '0 0 28 30';
			break;
		case 'close':
			path =
				'M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z';
			viewBox = '0 0 24 20';
			break;
		default:
			path = '';
	}

	if ( path === '' ) {
		return null;
	}

	const svgProps = {
		viewBox,
		'aria-hidden': true,
		focusable: false,
		role: 'img',
		...restProps,
	};

	return (
		<svg { ...svgProps }>
			<path d={ path } />
		</svg>
	);
}

Icon.defaultProps = {
	height: 20,
	width: 20,
};

Icon.propTypes = {
	height: PropTypes.number,
	width: PropTypes.number,
	icon: PropTypes.string.isRequired,
};
