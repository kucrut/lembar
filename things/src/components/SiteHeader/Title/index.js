import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import decode from 'simple-entity-decode';

import './style.pcss';

export default function Title( { children, url } ) {
	return (
		<h1 className="site-title">
			<Link className="site-title-link" to={ url }>{ decode( children ) }</Link>
		</h1>
	);
}

Title.defaultProps = {
	url: '/',
};

Title.propTypes = {
	children: PropTypes.string.isRequired,
	url: PropTypes.string,
};
