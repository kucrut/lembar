import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { NavMenu } from 'malimbu/components';

import Description from './Description';
import Title from './Title';

export function SiteHeader( props ) {
	const { description, title } = props;

	return (
		<header className="site-header" role="banner">
			<NavMenu id="main-menu" className="main-menu" location="main" />
			<Title>{ title }</Title>
			{ description && <Description>{ description }</Description> }
		</header>
	);
}

SiteHeader.defaultProps = {
	description: '',
};

SiteHeader.propTypes = {
	title: PropTypes.string.isRequired,
	description: PropTypes.string,
};

export default connect( state => ( {
	description: state.info.description,
	title: state.info.name,
} ) )( SiteHeader );
