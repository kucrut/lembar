import React from 'react';
import PropTypes from 'prop-types';

import './style.pcss';

export default function Description( { children } ) {
	return <p className="site-description">{ children }</p>;
}

Description.propTypes = {
	children: PropTypes.string.isRequired,
};
