import React from 'react';
import PropTypes from 'prop-types';

import './style.pcss';

export default function Main( props ) {
	const { children, ...rest } = props;

	return (
		<main role="main" { ...rest }>
			{ children }
		</main>
	);
}

Main.defaultProps = {
	className: 'wrapper',
	id: 'site-content',
};

Main.propTypes = {
	children: PropTypes.any.isRequired,
	className: PropTypes.string,
	id: PropTypes.string,
};
