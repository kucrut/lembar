<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<script>(function(c){c.add('js');c.remove('no-js')})(document.documentElement.classList)</script>
	<?php wp_head(); ?>
</head>
<body>
<?php do_action( 'lembar_render' ); ?>
<?php wp_footer(); ?>
</body>
</html>
